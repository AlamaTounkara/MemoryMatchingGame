package com.example.alama.picturematchingmemory.domain.usecases;

import com.example.alama.picturematchingmemory.domain.models.GamePhoto;

/**
 * Created by alamatounkara on 3/18/18.
 */

public interface IPhotoTapped extends IBaseUseCase {

    public interface CallBack {
        public void onFirsPhotoSelected(GamePhoto currentGamePhoto);
        public void onSecondPhotoSelected(GamePhoto selectedGamePhoto,
                                          GamePhoto currentGamePhoto, int numberOfTrial);
    }
    public void onPhotoPicked(int position,  GamePhoto currentGamePhoto);
    int saveSuccessPhotoID(String id);
    void resetPositionAndCurrentSelectedCell();
    void resetGame();
}
