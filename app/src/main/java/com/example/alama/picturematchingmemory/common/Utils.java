package com.example.alama.picturematchingmemory.common;

import com.google.gson.Gson;

/**
 * Created by alamatounkara on 3/18/18.
 */

public class Utils {

    public static <T> T jsonToSimpleObject(String json, Class<T> tClass ){
        return new Gson().fromJson(json, tClass);
    }
}

