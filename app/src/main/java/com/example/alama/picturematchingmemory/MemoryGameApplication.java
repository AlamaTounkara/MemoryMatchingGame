package com.example.alama.picturematchingmemory;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by alamatounkara on 3/18/18.
 */

public class MemoryGameApplication extends Application {

    private RequestQueue mRequestQueue;
    private static MemoryGameApplication sMemoryGameApplication;
    @Override
    public void onCreate() {
        super.onCreate();
        sMemoryGameApplication = this;
        mRequestQueue = Volley.newRequestQueue(this);
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public static MemoryGameApplication getInstance(){
        return sMemoryGameApplication;
    }

}
