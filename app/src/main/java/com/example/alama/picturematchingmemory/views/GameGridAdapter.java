package com.example.alama.picturematchingmemory.views;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.alama.picturematchingmemory.R;
import com.example.alama.picturematchingmemory.common.Constants;
import com.example.alama.picturematchingmemory.domain.models.GamePhoto;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by alamatounkara on 3/18/18.
 */

public class GameGridAdapter extends BaseAdapter {
    private Context mContext;
    private List<GamePhoto> mGamePhotos;



    public GameGridAdapter(Context context, List<GamePhoto> gamePhotos) {
        mContext = context;
        mGamePhotos = gamePhotos;
        Collections.shuffle(mGamePhotos);//let shuffle the photos
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return mGamePhotos.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return mGamePhotos.get(position);
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.game_grid_cell, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        setUpGridCell(position, viewHolder);
        return convertView;
    }

    private void setUpGridCell(final int position, ViewHolder viewHolder) {
        viewHolder.mGameCellImageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 300));
        final ViewHolder finalViewHolder = viewHolder;
        Picasso.with(mContext).load(mGamePhotos.get(position).getSource()).into(viewHolder.mGameCellImageView, new Callback() {
            @Override
            public void onSuccess() {
                final Handler handler = new Handler();
                //let user see the image for 2 seconds then hide the original image(face down)
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mGamePhotos.get(position).setDrawable(finalViewHolder.mGameCellImageView.getDrawable());
                        finalViewHolder.mGameCellImageView.setTag(mGamePhotos.get(position));
                        finalViewHolder.mGameCellImageView.setImageResource(Constants.GAME_PHOTO_FACE_DOWN_DRAWABLE_ID);
                    }
                }, 2000);
            }

            @Override
            public void onError() {

            }
        });
    }

    private class ViewHolder {
        ImageView mGameCellImageView;

        ViewHolder(View view) {
            this.mGameCellImageView = view.findViewById(R.id.game_cell_imageView);
        }
    }
}
