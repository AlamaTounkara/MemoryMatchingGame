package com.example.alama.picturematchingmemory.repository;

/**
 * Created by alamatounkara on 3/18/18.
 */

public interface OnServiceResponse {
    public void run(Object object);
}
