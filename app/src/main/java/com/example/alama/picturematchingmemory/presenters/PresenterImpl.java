package com.example.alama.picturematchingmemory.presenters;

import com.example.alama.picturematchingmemory.domain.models.GamePhoto;
import com.example.alama.picturematchingmemory.domain.usecases.IPhotoTapped;
import com.example.alama.picturematchingmemory.domain.usecases.PhotoTappedUseCaseImpl;

/**
 * Created by alamatounkara on 3/18/18.
 */

public class PresenterImpl implements IBasePresenter , IPhotoTapped.CallBack {

    private  IBasePresenter.IView mIView;
    private IPhotoTapped mPhotoTappedUseCase;

    public PresenterImpl(IView IView) {
        mIView = IView;
        mPhotoTappedUseCase = new PhotoTappedUseCaseImpl(this);
    }

    @Override
    public void photoTapped(int position, GamePhoto selectedGamePhoto) {
        mPhotoTappedUseCase.onPhotoPicked(position,selectedGamePhoto);
    }


    @Override
    public void onFirsPhotoSelected(GamePhoto selectedGamePhoto) {
        mIView.onFirstPhotoPickedSuccess(selectedGamePhoto);
    }

    @Override
    public void onSecondPhotoSelected(GamePhoto selectedGamePhoto,
                                      GamePhoto currentGamePhoto,  int numberOfTrial) {
        mIView.onSecondPhotoPickedSuccess(selectedGamePhoto,currentGamePhoto,numberOfTrial);
    }


    @Override
    public int saveSuccessPhotoID(String id) {
        return mPhotoTappedUseCase.saveSuccessPhotoID(id);
    }

    @Override
    public void resetPositionAndCurrentSelectedCell() {
        mPhotoTappedUseCase.resetPositionAndCurrentSelectedCell();
    }

    @Override
    public void resetGame() {
        mPhotoTappedUseCase.resetGame();
    }
}
