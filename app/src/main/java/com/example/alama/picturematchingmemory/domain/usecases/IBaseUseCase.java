package com.example.alama.picturematchingmemory.domain.usecases;

/**
 * Created by alamatounkara on 3/18/18.
 */

public interface IBaseUseCase {
    public void execute(Object object);
}
