package com.example.alama.picturematchingmemory.repository;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.alama.picturematchingmemory.MemoryGameApplication;

/**
 * Created by alamatounkara on 3/18/18.
 */

public class ServiceTask {
    private OnServiceResponse mOnServiceResponse;
    private String mEndpoint;

    public ServiceTask(String endpoint, OnServiceResponse onServiceResponse) {
        mOnServiceResponse = onServiceResponse;
        mEndpoint = endpoint;
    }

    /**
     * Very simple service call
     */
    public void call() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, mEndpoint, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mOnServiceResponse.run(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mOnServiceResponse.run(error);
            }
        });

        MemoryGameApplication.getInstance().getRequestQueue().add(stringRequest); //add request to queue
    }
}
