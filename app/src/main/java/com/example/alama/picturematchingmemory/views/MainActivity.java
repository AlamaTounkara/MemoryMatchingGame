package com.example.alama.picturematchingmemory.views;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.example.alama.picturematchingmemory.R;
import com.example.alama.picturematchingmemory.common.Constants;
import com.example.alama.picturematchingmemory.common.Utils;
import com.example.alama.picturematchingmemory.domain.models.GamePhoto;
import com.example.alama.picturematchingmemory.domain.models.Photo;
import com.example.alama.picturematchingmemory.domain.models.PhotoDetails;
import com.example.alama.picturematchingmemory.domain.models.PhotoSize;
import com.example.alama.picturematchingmemory.domain.models.PhotoSizeDetails;
import com.example.alama.picturematchingmemory.domain.models.RecentPhoto;
import com.example.alama.picturematchingmemory.domain.models.Size;
import com.example.alama.picturematchingmemory.presenters.IBasePresenter;
import com.example.alama.picturematchingmemory.presenters.PresenterImpl;
import com.example.alama.picturematchingmemory.repository.OnServiceResponse;
import com.example.alama.picturematchingmemory.repository.PhotoProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO:
 * GIVEN MORE TIMES I WOULD WANT DO MORE TESTING AND WRITE UNIT TEST CASES.
 * I WOULD ALSO ADD ANIMATION TO FLIP THE IMAGE FACE DOWN.
 *
 */
public class MainActivity extends AppCompatActivity implements IBasePresenter.IView {
    private PhotoProvider mPhotoProvider;
    private View mSelectedCell;
    private View mCurrentCell;
    private TextView mNumberOfTrialTV;
    private TextView mSuccessfulTrialTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(R.string.app_title);
        init();
    }


    private void init() {
        mPresenter = new PresenterImpl(this);
        mPhotoProvider = new PhotoProvider();
        mNumberOfTrialTV = findViewById(R.id.game_number_of_trial_textView);
        mSuccessfulTrialTV = findViewById(R.id.success_trial_textView);
        mNumberOfTrialTV.setText(MainActivity.this.getResources().getString(R.string.number_of_trial, 0));
        mSuccessfulTrialTV.setText(MainActivity.this.getResources().getString(R.string.success_trial, 0));

        downloadGamePhotos();
    }

    /**
     * Get the list of rcents photos and  display them in a gridview.
     * If service call fails for whatever reason, allow them to retry.
     */
    private void downloadGamePhotos() {
        mPhotoProvider.getRecentPhotos(new OnServiceResponse() {
            @Override
            public void run(Object object) {

                if (object == null || (object instanceof VolleyError)) {
                    showFlickerPhotoServiceAPICallFailAlert();
                    return;
                }

                RecentPhoto recentPhoto = Utils.jsonToSimpleObject((String) object, RecentPhoto.class);

                PhotoDetails photoDetails = recentPhoto == null ? null : recentPhoto.getPhotos();
                if (photoDetails == null || photoDetails.getPhoto() == null || photoDetails.getPhoto().isEmpty()) {
                    showFlickerPhotoServiceAPICallFailAlert();
                    return;
                }
                //doing back to back call to get the other photo element(i:e width,height, url etc..)
                getPhotoSize(photoDetails.getPhoto());
            }
        });
    }

    /**
     * The Flicker getSizes API is not giving me an options to provide multiple ID, Hence
     * I am calling multiple time to get a set of picture to fill my 4xd gridview
     *
     * @param photoList
     */
    private void getPhotoSize(List<Photo> photoList) {
        List<GamePhoto> gamePhotos = new ArrayList<>();
        int i = 0;
        downloadPhotoSizes(gamePhotos, photoList, i);
    }


    /**
     * Get the list of  photos and  display them in a gridview.
     * If service call fails for whatever reason, allow them to retry.
     * <p>
     * Since We are using 4x4 grid,that is a total of 16 images. As a result, in our Matching memory game,
     * we want 8 unique images and we will then duplicate each one to make the total to 16 and display
     * them in our 4x4 grid
     */
    private void downloadPhotoSizes(final List<GamePhoto> gamePhotos, final List<Photo> photoList, final int index) {
        if (gamePhotos.size() == Constants.TOTAL_IMAGE_FOR_THE_GAME) {//8 is the total we want
            setUpGridView(gamePhotos);
            return;
        }

        if (!shouldWeMakeServiceCall(photoList, index)) return;

        mPhotoProvider.getPhotoSizes(photoList.get(index).getId(), new OnServiceResponse() {
            @Override
            public void run(Object object) {




                if (object == null || (object instanceof VolleyError)) {
                    showFlickerPhotoServiceAPICallFailAlert();
                    return;
                }

                PhotoSize photoSize = Utils.jsonToSimpleObject((String) object, PhotoSize.class);

                PhotoSizeDetails photoSizeDetails = photoSize == null ? null : photoSize.getSizes();
                List<Size> sizeList = photoSizeDetails == null ? null : photoSizeDetails.getSize();

                if (sizeList == null || sizeList.isEmpty()) {
                    showFlickerPhotoServiceAPICallFailAlert();
                    return;
                }

                String sourceUrl = getPhotoSourceUrl(sizeList);

                createGamePhotoObj(sourceUrl, photoList.get(index), gamePhotos);
                int tempIndex = index + 1;
                downloadPhotoSizes(gamePhotos, photoList, tempIndex);
            }
        });
    }

    private boolean shouldWeMakeServiceCall(List<Photo> photoList, int index) {
        if (index >= photoList.size()) {
            showFlickerPhotoServiceAPICallFailAlert();//we can't get all the all the 8 uniques pictures
            return false;
        }

        final Photo photo = photoList.get(index);
        if (photo == null || TextUtils.isEmpty(photo.getId())) {
            showFlickerPhotoServiceAPICallFailAlert();
            return false;
        }
        return true;
    }

    private void createGamePhotoObj(String sourceUrl, Photo photo, List<GamePhoto> gamePhotos) {
        GamePhoto gamePhoto = new GamePhoto();
        gamePhoto.setId(photo.getId());
        gamePhoto.setSource(sourceUrl);
        gamePhotos.add(gamePhoto);
        gamePhotos.add(gamePhoto);
    }

    @Nullable
    private String getPhotoSourceUrl(List<Size> sizeList) {
        Size size = null;
        for (int i = 0; i < sizeList.size(); i++) {
            Size tempSize = sizeList.get(i);
            if (tempSize != null && tempSize.getLabel() != null
                    && tempSize.getLabel().equalsIgnoreCase("Medium")) {
                size = sizeList.get(i);
                break;
            }
        }
        String sourceUrl = null;
        if (size != null) {
            sourceUrl = size.getSource();
        }
        if (TextUtils.isEmpty(sourceUrl)) {
            sourceUrl = sizeList.get(0) != null ? sizeList.get(0).getUrl() : null;
        }
        return sourceUrl;
    }

    IBasePresenter mPresenter;


    private void setUpGridView(final List<GamePhoto> gamePhotos) {
        final GridView gridView = findViewById(R.id.game_grid_view);
        GameGridAdapter gameGridAdapter = new GameGridAdapter(this, gamePhotos);
        gridView.setAdapter(gameGridAdapter);

        setUpButtons(gamePhotos, gridView);

        setGridViewCellClickListener(gridView);
    }

    private void setUpButtons(final List<GamePhoto> gamePhotos, final GridView gridView) {
        final Button retrySamePhotosBTN = findViewById(R.id.retry_same_photos_BTN);
        retrySamePhotosBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.resetGame();
                GameGridAdapter gameGridAdapter = new GameGridAdapter(MainActivity.this, gamePhotos);
                gridView.setAdapter(gameGridAdapter);
                gameGridAdapter.notifyDataSetChanged();
            }
        });

        final Button retryDifferentPhotosBTN = findViewById(R.id.retry_different_photos_BTN);
        retryDifferentPhotosBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.resetGame();
                downloadGamePhotos();
            }
        });
    }

    private void setGridViewCellClickListener(GridView gridView) {
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
                GamePhoto currentGamePhoto = null;
                mCurrentCell = view;
                final ImageView currentImageView = (ImageView) ((LinearLayout) mCurrentCell).getChildAt(0);
                if (currentImageView.getTag() instanceof GamePhoto) {
                    currentGamePhoto = (GamePhoto) currentImageView.getTag();
                }

                mPresenter.photoTapped(position, currentGamePhoto);
            }
        });
    }

    private void showImageFaceDown(ImageView selectedImageView, ImageView currentImageView) {
        selectedImageView.setImageResource(Constants.GAME_PHOTO_FACE_DOWN_DRAWABLE_ID);
        currentImageView.setImageResource(Constants.GAME_PHOTO_FACE_DOWN_DRAWABLE_ID);
    }

    private void showSuccessSelection(ImageView selectedImageView, ImageView currentImageView) {
        selectedImageView.setImageResource(R.drawable.ic_mood_black_24px);
        currentImageView.setImageResource(R.drawable.ic_mood_black_24px);
    }

    private void resetPositionAndCurrentSelectedCell() {
        mSelectedCell = null;
        mPresenter.resetPositionAndCurrentSelectedCell();
    }


    private void showFlickerPhotoServiceAPICallFailAlert() {
        showAlertMessage(getString(R.string.photo_call_failure_msg), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                downloadGamePhotos();
                dialog.cancel();
            }
        });
    }


    /**
     * Used to show an Alert dialog when the service call fails with any additional message you want.
     *
     * @param message                is the message to display to in your alert dialog. If null, the content of the alert
     *                               dialog will be empty
     * @param positiveButtonListener is a listener to be invoke when the position button is tap.
     *                               If it is null, there will be no positive button on alert dialog
     */
    private void showAlertMessage(String message, DialogInterface.OnClickListener positiveButtonListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.matching_game_text);
        builder.setMessage(message);

        if (positiveButtonListener != null) {
            builder.setPositiveButton(R.string.try_again, positiveButtonListener);
        }

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    @Override
    public void onFirstPhotoPickedSuccess(GamePhoto selectedGamePhoto) {
        final ImageView currentImageView = (ImageView) ((LinearLayout) mCurrentCell).getChildAt(0);
        currentImageView.setImageDrawable(selectedGamePhoto.getDrawable());//this has to be a valid drawable
        mSelectedCell = mCurrentCell;
    }

    @Override
    public void onSecondPhotoPickedSuccess(final GamePhoto selectedGamePhoto,
                                           final GamePhoto currentGamePhoto, int numberOfTrial) {

        final ImageView currentImageView = (ImageView) ((LinearLayout) mCurrentCell).getChildAt(0);

        currentImageView.setImageDrawable(currentGamePhoto.getDrawable());
        final Handler handler = new Handler();
        if (mSelectedCell == null) return;

        mNumberOfTrialTV.setText(MainActivity.this.getResources().getString(R.string.number_of_trial, numberOfTrial));


        final ImageView selectedImageView = (ImageView) ((LinearLayout) mSelectedCell).getChildAt(0);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSelectedCell == null) return;
                if (selectedGamePhoto.getId().equalsIgnoreCase(currentGamePhoto.getId())) {
                    //the 2 images matches
                    showSuccessSelection(selectedImageView, currentImageView);
                    mCurrentCell.setBackgroundColor(MainActivity.this.getResources().getColor(android.R.color.white));
                    mSelectedCell.setBackgroundColor(MainActivity.this.getResources().getColor(android.R.color.white));

                   int countSuccessfulTrial =  mPresenter.saveSuccessPhotoID(selectedGamePhoto.getId());

                    mSuccessfulTrialTV.setText(MainActivity.this.getResources().getString(R.string.success_trial, countSuccessfulTrial));

                    //let reset our values
                    resetPositionAndCurrentSelectedCell();
                    return;
                }

                showImageFaceDown(selectedImageView, currentImageView);
                //let reset our values
                resetPositionAndCurrentSelectedCell();
            }
        }, 500);
    }
}