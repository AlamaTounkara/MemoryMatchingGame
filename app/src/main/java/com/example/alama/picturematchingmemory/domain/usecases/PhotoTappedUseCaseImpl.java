package com.example.alama.picturematchingmemory.domain.usecases;

import com.example.alama.picturematchingmemory.domain.models.GamePhoto;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by alamatounkara on 3/18/18.
 */

public class PhotoTappedUseCaseImpl implements IPhotoTapped {
    private Stack<GamePhoto> mSelectedPhotos = new Stack<>();
    private List<String> mSuccessPhotoIDs = new ArrayList<>();
    private int mCurrentImagePosition = -1;
    private int mCountTrial;
    private int mCountSuccessfulTrial;
    private  IPhotoTapped.CallBack mPresenter;


    public PhotoTappedUseCaseImpl(CallBack presenter) {
        mPresenter = presenter;
    }

    @Override
    public void execute(Object object) {

    }

    @Override
    public void onPhotoPicked(int position, GamePhoto currentGamePhoto) {

        if (!isCurrentPositionSelectable(position, currentGamePhoto)) return;


        ///////user picks the first image, let make sure we record that
        if (mSelectedPhotos.isEmpty()) {
            mCurrentImagePosition = position;
            mSelectedPhotos.push(currentGamePhoto);
            mPresenter.onFirsPhotoSelected(currentGamePhoto);
            return;
        }
        mPresenter.onSecondPhotoSelected(mSelectedPhotos.pop(),currentGamePhoto, ++mCountTrial);
    }

    @Override
    public int saveSuccessPhotoID(String id) {
        mSuccessPhotoIDs.add(id);
       return  ++mCountSuccessfulTrial;
    }

    @Override
    public void resetPositionAndCurrentSelectedCell() {
        mCurrentImagePosition = -1;
    }

    @Override
    public void resetGame() {
        mCountTrial = 0;
        mCountSuccessfulTrial = 0;
    }

    /**
     * - If image was flipped over as a result of successful match, we don't want to do anything
     * - If cell is already selected, user needs to select a different pic
     * - Game is also over when 'mCountSuccessfulTrial == 8'
     * @param position is the cell just tapped
     * @param currentGamePhoto is the current photo data associated with tapped cell
     * @return true is we want to proceed with game logics or false if the particular cell should
     * not do anything at this moment.
     */
    private boolean isCurrentPositionSelectable(int position, GamePhoto currentGamePhoto) {
        return (currentGamePhoto != null &&
                !mSuccessPhotoIDs.contains(currentGamePhoto.getId())
                && mCurrentImagePosition != position
                && mCountSuccessfulTrial < 8);
    }
}
