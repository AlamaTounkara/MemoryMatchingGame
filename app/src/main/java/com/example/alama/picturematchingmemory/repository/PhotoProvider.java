package com.example.alama.picturematchingmemory.repository;

import com.example.alama.picturematchingmemory.common.Constants;

/**
 * Created by alamatounkara on 3/18/18.
 */

public class PhotoProvider {

    /**
     * Make a service call to get the list size(which contains photo url)
     *
     * @param onServiceResponse is a callback that will be executed when
     *                              the service call return(error or success)
     */
    public void getPhotoSizes(String photoID, OnServiceResponse onServiceResponse){
        String url = getUrlString(Constants.FLICKER_PHOTO_SIZES_ENDPOINT)+"&photo_id="+photoID;

        ServiceTask serviceTask = new ServiceTask(url, onServiceResponse);
        serviceTask.call();
    }


    /**
     * Make a service call to get the list of recent photos.
     *
     * @param onServiceResponse is a callback that will be executed when
     *                              the service call return(error or success)
     */
    public void getRecentPhotos(OnServiceResponse onServiceResponse){

        String url = getUrlString(Constants.FLICKER_RECENT_PHOTO_ENDPOINT)+"&extras=kitten";

        ServiceTask serviceTask = new ServiceTask(url, onServiceResponse);
        serviceTask.call();
    }


    private String getUrlString(String method){
        return Constants.FLICKER_API_BASE+method+"&api_key="+Constants.FLICKER_API_KEY+"&format=json&nojsoncallback=1";
    }
}
