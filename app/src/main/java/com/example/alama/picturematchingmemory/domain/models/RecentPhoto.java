package com.example.alama.picturematchingmemory.domain.models;

/**
 * Created by alamatounkara on 3/18/18.
 */

public class RecentPhoto {
    private PhotoDetails photos;
    private String stat;


    public PhotoDetails getPhotos() {
        return photos;
    }

    public void setPhotos(PhotoDetails photos) {
        this.photos = photos;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
}
