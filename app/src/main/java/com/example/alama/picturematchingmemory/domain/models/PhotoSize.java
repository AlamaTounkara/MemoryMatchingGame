package com.example.alama.picturematchingmemory.domain.models;

/**
 * Created by alamatounkara on 3/18/18.
 */

public class PhotoSize {
    private PhotoSizeDetails sizes;
    private String stat;

    public PhotoSizeDetails getSizes() {
        return sizes;
    }

    public void setSizes(PhotoSizeDetails sizes) {
        this.sizes = sizes;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
}
