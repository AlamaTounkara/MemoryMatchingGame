package com.example.alama.picturematchingmemory.presenters;

import com.example.alama.picturematchingmemory.domain.models.GamePhoto;
import com.example.alama.picturematchingmemory.views.IBaseView;

/**
 * Created by alamatounkara on 3/18/18.
 */

public interface IBasePresenter {
    interface IView extends IBaseView {
        public void onFirstPhotoPickedSuccess(GamePhoto selectedGamePhoto);
        public void onSecondPhotoPickedSuccess(GamePhoto selectedGamePhoto,
                                               GamePhoto currentGamePhoto, int numberOfTrial);
    }

    void photoTapped(int position, GamePhoto selectedGamePhoto);
    int saveSuccessPhotoID(String id);
    void resetPositionAndCurrentSelectedCell();
    void resetGame();
}
