package com.example.alama.picturematchingmemory.domain.models;

import android.graphics.drawable.Drawable;

/**
 * Created by alamatounkara on 3/18/18.
 */

public class GamePhoto {
    private String id;
    private String source;
    private String url;
    private Drawable mDrawable;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public Drawable getDrawable() {
        return mDrawable;
    }

    public void setDrawable(Drawable drawable) {
        mDrawable = drawable;
    }

}
