package com.example.alama.picturematchingmemory.common;

import com.example.alama.picturematchingmemory.R;

/**
 * Created by alamatounkara on 3/18/18.
 */

public class Constants {
    public final static String FLICKER_API_KEY = "5423dbab63f23a62ca4a986e7cbb35e2";
    public final static String FLICKER_API_BASE = "https://api.flickr.com/services/rest";
    public final static String FLICKER_RECENT_PHOTO_ENDPOINT = "/?method=flickr.photos.getRecent";
    public final static String FLICKER_PHOTO_SIZES_ENDPOINT = "/?method=flickr.photos.getSizes";
    public final static int TOTAL_IMAGE_FOR_THE_GAME = 16;
    public final static int GAME_PHOTO_FACE_DOWN_DRAWABLE_ID = R.drawable.ic_casino_white_24px;
}
